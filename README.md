# Voidpack
A repo for the Voidpack resource pack for Minecraft 1.12 -- 1.13 soon™

## [Download found here](https://github.com/oitsjustjose/Voidpack/archive/1.12.x.zip)

This will download the latest version of this repo for you and can be used directly in Minecraft

## Installation:

Open Minecraft, select "Options" → "Resource Packs" → "Open Resource Packs Folder" and paste the downloaded .zip file in

## Credits:

Most resources are from CyanideX's Unity Resource Pack *or* Jappa's new 1.14 Resources, that have been backported using a customized Java-made tool. A few are even made by myself!